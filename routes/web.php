<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\ReplyController;
use App\Http\Middleware\CheckAuth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;
use App\Http\Controllers\ListController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FormController::class, 'create'])->name('get-form');

Route::post('/', [FormController::class, 'store'])->name('set-form');

Route::get('/list', [ListController::class, 'create'])->middleware(CheckAuth::class);

Route::get('/login', [LoginController::class, 'create']);
Route::post('/login', [LoginController::class, 'login']);

Route::get('/reply', [ReplyController::class, 'create'])->middleware(CheckAuth::class);
Route::post('/reply', [ReplyController::class, 'reply'])->name('set-reply');
