<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <div class="login-form">
        <form method="post">
            @csrf
            <p><strong>E-mail:</strong>
                <label>
                    <input maxlength="50" size="40" name="email">
                </label></p>
            <p><strong>Пароль:</strong>
                <label>
                    <input type="password" maxlength="25" size="40" name="password">
                </label></p>
            <input type="submit" value="Отправить" class="login-btn">
        </form>
        <div class="err-login">
            @if(isset($err_login))
                <h2>{{ $err_login }}</h2>
            @endif
        </div>
    </div>
</body>
</html>
