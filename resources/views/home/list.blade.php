<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Лист</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <div class="main-div-table">
        <h1>Лист форм:</h1><br>
        <a href="/reply">Ответить</a>
        <table class="main-table">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Тема</th>
                    <th scope="col">Сообщение</th>
                    <th scope="col">Имя</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Время</th>
                    <th scope="col">Ответ</th>
                </tr>
            </thead>
            <tbody>
                @foreach($form_data as $tr)
                    <tr>
                    @foreach($tr as $td)
                        <td>{{ $td }}</td>
                    @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>
