<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Форма</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <div class="main-header">
        <h1>Заполните форму:</h1><br>
    </div>
    <div class="main-form">
        <form action="{{ route('set-form') }}" method="post">
            @csrf
            <label>
                <p>Тема: </p>
                <input name="theme" type="text">
            </label>
            <label>
                <p>Сообщение: </p>
                <input name="msg" type="text">
            </label>
            <label>
                <p>Имя: </p>
                <input name="name" type="text">
            </label>
            <label>
                <p>E-mail: </p>
                <input name="email" type="email">
            </label><br><br>
            <input type="submit" value="Отправить">
        </form>

        {{--    Вывод ошибки валидации формы --}}
        <br>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (isset($time_valid))
            <div class="alert alert-danger">
                <br>
                <h3>Форму можно отправлять только раз в сутки.</h3>
                <br>
            </div>
        @endif
    </div>
    <div class="login-link">
        <a href="/login"><h4>Войти</h4></a>
    </div>
</body>
</html>
