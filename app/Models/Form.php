<?php

namespace App\Models;

use App\Http\Requests\StoreFormRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Form extends Model
{
    use HasFactory;

    private const secIn24h = 86400;

    private function checkTimestamp(string $email): bool
    {
        if (DB::table('forms')->where('email', $email)->exists()) {
            $last_timestamp = DB::table('forms')
                ->select('email', 'created_at')
                ->where('email', $email)
                ->max('created_at');

            if (time() - strtotime($last_timestamp) < self::secIn24h) {
                return false;
            }
        }

        return true;
    }

    public function getAllForms(): Collection
    {
        return DB::table('forms')->get();
    }

    public function storeFormInDB(StoreFormRequest $request)
    {
        if ($this->checkTimestamp($request->get('email'))) {
                DB::table('forms')->insert([
                    'theme' => $request->get('theme'),
                    'msg' => $request->get('msg'),
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'created_at' => date("Y-m-d H:i:s"),
                ]);

                return redirect('login'); //нужно это делать в контроллере
        } else {
            return view('home.form', ['time_valid' => true]); // как и это
        }
    }

    public function getThemeByEmail(string $email): string
    {
        return DB::table('forms')
            ->select('theme')
            ->where('email', $email)
            ->max('created_at');
    }
}
