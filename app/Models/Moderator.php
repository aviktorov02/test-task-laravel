<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Moderator extends Model
{
    use HasFactory;

    public static function getHashByEmail(string $email): string
    {
        return DB::table('moderators')
            ->select('passwd_hash')
            ->where('email', $email)
            ->max('passwd_hash');
    }
}
