<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    public function create()
    {
        return view('home.reply');
    }

    public function reply(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email',
            'msg' => 'required',
        ]);

        if ($validated) $this->sendEmail($request);

        return redirect('/list');
    }

    private function sendEmail(Request $request)
    {
//        Mail::to($request->get('email'))->send(new FeedbackMail($request->get('msg')));

        $email = $request->get('email');

        $form = new Form;
        $theme = $form->getThemeByEmail($email);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";

        $headers .= "From: " . "<$email>\r\n";
        mail($email, $theme, $request->get('msg'), $headers);
    }
}
