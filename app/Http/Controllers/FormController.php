<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreFormRequest;
use App\Models\Form;

class FormController extends Controller
{
    public function create()
    {
        return view('home.form');
    }

    public function store(StoreFormRequest $request)
    {
        $validated = $request->validated();

        if ($validated) {
            $form = new Form();
            return $form->storeFormInDB($request);
        }

        return null;
    }
}
