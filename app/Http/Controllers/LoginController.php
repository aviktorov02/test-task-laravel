<?php

namespace App\Http\Controllers;

use App\Models\Moderator;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    public function create()
    {
        return view('home.login');
    }

    public function login(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validated) {
            $hash = Moderator::getHashByEmail($request->input('email'));

            if ($hash == hash('sha256', $request->input('password'))) {
                session(['login' => true]);
                session(['moderator_email' => $request->input('email')]);
                return redirect('/list');
            }
        }

        return view('home.login', ['err_login' => 'Ошибка входа.']);
    }
}
