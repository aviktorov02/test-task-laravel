<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Models\Form;
use Illuminate\Support\Collection;

class ListController extends Controller
{
    public function create()
    {
        return view('home.list', ['form_data' => $this->getForms()]);
    }

    private function getForms(): Collection
    {
        $form = new Form;
        return $form->getAllForms();
    }
}
