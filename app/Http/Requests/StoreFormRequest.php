<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true; // debug
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'theme' => 'required|max:255',
            'msg' => 'required',
            'name' => 'required|max:255',
            'email' => 'required|email',
        ];
    }

    public function messages(): array
    {
        return [
            'theme.required' => 'A theme is required',
            'msg.required' => 'A msg is required',
            'name.required' => 'A name is required',
            'email.required' => 'A email is required',
        ];
    }
}
