<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('forms')->insert([
                'theme' => Str::random(10),
                'msg' => Str::random(100),
                'name' => Str::random(5),
                'email' => Str::random(10) . '@gmail.com',
                'created_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
